module.exports = {
    bracketSpacint: true,
    singleQuote: true,
    tabWidth: 2,
    useTabs: false,
    trailingComma: "none",
    semi: false,
};