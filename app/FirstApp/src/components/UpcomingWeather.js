
import React from 'react'
import { SafeAreaView, StatusBar, StyleSheet, FlatList, View, Text } from 'react-native';
import { Feather } from '@expo/vector-icons'

const DATA = [
    {
        dt_txt: "2023-05-23 16:00:00",
        main: {
            temp_min: 18,
            temp_max: 25,
    },
    weather: [
            {
                main: 'Clear'
            }
        ]
    },
    {
        dt_txt: "2023-05-23 17:00:00",
        main: {
            temp_min: 17,
            temp_max: 21,
    },
    weather: [
            {
                main: 'Clouds'
            }
        ]  
    },
    {
        dt_txt: "2023-05-23 18:00:00",
        main: {
            temp_min: 13,
            temp_max: 17,
    },
    weather: [
            {
                main: 'Rain'
            }
        ]  
    }
]

const Item = (props) => {
    const { dt_txt, min, max, condition} = props
    return (
        <View>
            <Feather name = {'sun'} size = {50} color = {'white'} />
            <Text>{dt_txt}</Text>
            <Text>{min}</Text>
            <Text>{max}</Text>
        </View>
    )
}

const UpcomingWeather = ()=> {

    const renderItem = ({item}) => (
        <Item 
        condition = {item.weather[0].main}
        dt_txt={item.dt_txt}
        min = {item.main.temp_min}
        max = {item.main.temp_max}
        />
    )

    return (
        <SafeAreaView styles = {styles.container}>
            <Text>Próximo Clima</Text>
            <FlatList data={DATA} renderItem={renderItem} />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: StatusBar.currentHeight,
    }
})
export default UpcomingWeather;