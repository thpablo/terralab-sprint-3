
import { SafeAreaView, StyleSheet, Text, View, StatusBar } from 'react-native';
import { Feather } from '@expo/vector-icons';

const CurrentWeather = () => {
  return (
    <SafeAreaView style = {styles.wrapper}>
      <View style = {styles.headContainer}>
        <Feather name="sun" size={32} color="#e6ddbc" style = {{marginBottom: 12, marginTop: 12}}/>
        <Text style = {styles.temp}>12 Graus</Text>
        <View style = {styles.MaxMin}>
          <Text style = {styles.max}>Max: 15 Graus</Text>
          <Text style = {styles.min}>Min: 8 Graus</Text>
        </View>
      </View>
      <View style = {styles.bodyWrapper}>
        <Text style = {styles.description}>Está ensolarado</Text>
        <Text style = {styles.message}>Vá dar uma caminhada!</Text>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },

// ------ Head --------

  headContainer: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#262525',
    paddingTop: StatusBar.currentHeight,
  },

  MaxMin: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    gap: 15,
  },

  temp: {
    color: '#e6ddbc',
    fontSize: 35,
    marginBottom: 5,
  },

  max: {
    color: 'white',
  },

  min: {
    color: 'white',
  },

// ------- Body -------

bodyWrapper: {
  backgroundColor: '#525252',
  alignItems: 'center'
},

description: {
  fontSize: 24,
  color: '#e6ddbc',
},

message: {
  fontSize: 12,
  color: '#e6ddbc',
}

});


export default CurrentWeather;